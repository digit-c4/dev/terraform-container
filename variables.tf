variable "name" {
  type        = string
  default     = null
  description = "Container name, if none provided `image` will be used instead"
}

variable "image" {
  type        = string
  description = "Docker image"
}

variable "image_keep_locally" {
  type        = bool
  default     = true
  description = "Whether the image should stay on host after destroy"
}
variable "files_host_dir" {
  type        = string
  description = "The path to a directory where the files must be stored on the remote Docker host .Directory cannot pre-exist and will be erased when the module is desotryed"
}

variable "env" {
  type        = map(string)
  default     = {}
  description = "A mapping of environment variables and values"
}

variable "vault_path" {
  type        = string
  default     = null
  description = "default Vault path to use when not provided in one of the `vault_env` or `vault_files` entries. Required when an entry in those inputs is missing the `vault_path` attribute"
}

variable "vault_env" {
  type = map(object({
    vault_path = optional(string)
    secret_key = optional(string)
  }))
  description = "A mapping of environment variable and Vault key. Default value of `vault_path` attribute is the `vault_path` variable. Default `secret_key` is the entry key."
  default = {}
}


variable "vault_files" {
  type        = map(object({
    vault_path    = optional(string)
    secret_key    = optional(string)
    host_filename = optional(string)
    mount_path    = optional(string)
  }))
  default     = {}
  description = "Files to mount, content loaded from Vault. Default value of `vault_path` attribute is the `vault_path` variable. Default `secret_key` is the entry key. Default `host_filename` is the `sha256` hash of the entry key. Default `mount_path` is the entry key"
}

variable "files" {
  type        = map(object({
    content       = string
    host_filename = optional(string)
    mount_path    = optional(string)
  }))
  default     = {}
  description = "A mapping of file content to bind-mount on the container. The file countent is set to the mandatory `content` attribute of the mappin object, and the mount path is either the `mount_path` attribute if provided, or the mapping key. Before to be mounted, the files are created on the host machine in the `files_host_dir` and named after the key or the key hash"
}
