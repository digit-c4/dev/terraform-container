terraform {
  required_version = ">= 0.13"

  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }

    remote = {
      source = "widespot/remote"
      version = "0.2.1"
    }

    vault = {
      source = "hashicorp/vault"
      version = "3.23.0"
    }
  }
}

provider "remote" {
  host     = var.remote_host
  username = var.remote_username
  password = var.remote_password
}

provider "docker" {
  host     = "ssh://${var.remote_host}"
  ssh_opts = ["-o", "StrictHostKeyChecking=no", "-o", "UserKnownHostsFile=/dev/null"]
}

provider "vault" {
  address = var.vault_host
  token = var.vault_token
  namespace = var.vault_namespace

  # Required for use with NMS Vault because the tokens don't have the right to 
  # generate child token. (see doc https://registry.terraform.io/providers/hashicorp/vault/latest/docs#token)
  skip_child_token = true
}


variable remote_username {
  type = string
}

variable remote_password {
  type = string
}

variable remote_host {
  type = string
}

variable vault_host {
  type        = string
}

variable vault_token {
  type        = string
}

variable vault_namespace{
  type        = string
}

