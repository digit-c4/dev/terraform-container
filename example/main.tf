resource "vault_generic_secret" "example" {
  path = "cubbyhole/foo"

  data_json = jsonencode(
    {
      "/etc/nginx/conf.d/nginx.conf.example" = file("nginx.conf"),
      "DB_PASSWORD" = "UlTra5eCuR1t?"
    }
  )
}

module nginx {
  source = "../"
  
  name = "nms-nginx"
  image = "nginx"
  files_host_dir = "/home/joierap/test"

  vault_path = vault_generic_secret.example.path

  env = {
    "LOG_LEVEL": "DEBUG"
  }
  vault_env = {
    "DB_PASSWORD": {}
  }

  files = {
    "/opt/lib/test.crt" = {
      content = "ceci est un contenu"
    }
  }
  vault_files = {
    "/etc/nginx/conf.d/nginx.conf.example" = {}
  }

  # Secrets normally pre-exists. If not, the secret dependencies have to be
  # listed here, or the module will try to load it at terraform plan time
  depends_on = [
    vault_generic_secret.example
  ]
}
