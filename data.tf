data "vault_generic_secret" "secret" {
  for_each = toset(distinct(compact(
    concat([var.vault_path], [for k,v in var.vault_env: v.vault_path], [for k,v in var.vault_files: v.vault_path])
  )))

  path = each.value
}

locals {
  vault_env = [for k,v in var.vault_env: "${k}=${
    data.vault_generic_secret.secret[coalesce(v.vault_path, var.vault_path)].data[coalesce(v.secret_key, k)]
  }"]
}
