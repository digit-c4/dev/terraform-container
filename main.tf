resource "docker_image" "image" {
  name = var.image
  keep_locally = var.image_keep_locally
}

resource "remote_folder" "files_host_dir" {
  path    = var.files_host_dir
}

resource "remote_file" "vault_file" {
  for_each = var.vault_files

  content = data.vault_generic_secret.secret[coalesce(var.vault_path, var.vault_path)].data[coalesce(each.value.secret_key, each.key)]
  path    = "${remote_folder.files_host_dir.path}/${coalesce(each.value.host_filename, sha256(each.key))}"
}

resource "remote_file" "file" {
  for_each = var.files

  content = each.value.content
  path    = "${remote_folder.files_host_dir.path}/${coalesce(each.value.host_filename, sha256(each.key))}"
}

resource "docker_container" "container" {
  image = docker_image.image.image_id
  name  = coalesce(var.name, var.image)

  env = toset(concat([for k, v in var.env: "${k}=${v}"], local.vault_env))

  dynamic "volumes" {
    for_each = remote_file.file
    content {
      container_path = var.files[volumes.key].mount_path == null ? volumes.key : var.files[volumes.key].mount_path
      host_path = volumes.value.path
    }
  }

  dynamic "volumes" {
    for_each = remote_file.vault_file
    content {
      container_path = var.vault_files[volumes.key].mount_path == null ? volumes.key : var.vault_files[volumes.key].mount_path
      host_path = volumes.value.path
    }
  }
}
