# Remote container deployment using Terraform

Deploy a Docker container, with Vaulted mounted files and environment variables
```tf
module nginx {
  source = "git::https://code.europa.eu/digit-c4/dev/terraform-container.git?ref=v0.1.0"
  
  image = "nginx"

  vault_path = "cubbyhole/foo"
  files_host_dir = "/home/joierap/test"

  env = {
    "LOG_LEVEL": "DEBUG"
  }
  vault_env = {
    "DB_PASSWORD": {}
  }

  files = {
    "/opt/lib/test.crt" = {
      content = "blabetiblou"
    }
  }
  vault_files = {
    "/etc/nginx/conf.d/nginx.conf.example" = {
      secret_key = "nginx.conf"
    }
  }
}
```

## API Documentation

<!-- BEGIN_TF_DOCS -->
### Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_docker"></a> [docker](#requirement\_docker) | 3.0.2 |
| <a name="requirement_remote"></a> [remote](#requirement\_remote) | 0.2.1 |
| <a name="requirement_vault"></a> [vault](#requirement\_vault) | 3.23.0 |

### Providers

| Name | Version |
|------|---------|
| <a name="provider_docker"></a> [docker](#provider\_docker) | 3.0.2 |
| <a name="provider_remote"></a> [remote](#provider\_remote) | 0.2.1 |
| <a name="provider_vault"></a> [vault](#provider\_vault) | 3.23.0 |

### Modules

No modules.

### Resources

| Name | Type |
|------|------|
| [docker_container.container](https://registry.terraform.io/providers/kreuzwerker/docker/3.0.2/docs/resources/container) | resource |
| [docker_image.image](https://registry.terraform.io/providers/kreuzwerker/docker/3.0.2/docs/resources/image) | resource |
| [remote_file.file](https://registry.terraform.io/providers/widespot/remote/0.2.1/docs/resources/file) | resource |
| [remote_file.vault_file](https://registry.terraform.io/providers/widespot/remote/0.2.1/docs/resources/file) | resource |
| [remote_folder.files_host_dir](https://registry.terraform.io/providers/widespot/remote/0.2.1/docs/resources/folder) | resource |
| [vault_generic_secret.secret](https://registry.terraform.io/providers/hashicorp/vault/3.23.0/docs/data-sources/generic_secret) | data source |

### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_env"></a> [env](#input\_env) | A mapping of environment variables and values | `map(string)` | `{}` | no |
| <a name="input_files"></a> [files](#input\_files) | A mapping of file content to bind-mount on the container. The file countent is set to the mandatory `content` attribute of the mappin object, and the mount path is either the `mount_path` attribute if provided, or the mapping key. Before to be mounted, the files are created on the host machine in the `files_host_dir` and named after the key or the key hash | <pre>map(object({<br>    content       = string<br>    host_filename = optional(string)<br>    mount_path    = optional(string)<br>  }))</pre> | `{}` | no |
| <a name="input_files_host_dir"></a> [files\_host\_dir](#input\_files\_host\_dir) | The path to a directory where the files must be stored on the remote Docker host .Directory cannot pre-exist and will be erased when the module is desotryed | `string` | n/a | yes |
| <a name="input_image"></a> [image](#input\_image) | Docker image | `string` | n/a | yes |
| <a name="input_image_keep_locally"></a> [image\_keep\_locally](#input\_image\_keep\_locally) | Whether the image should stay on host after destroy | `bool` | `true` | no |
| <a name="input_name"></a> [name](#input\_name) | Container name, if none provided `image` will be used instead | `string` | `null` | no |
| <a name="input_vault_env"></a> [vault\_env](#input\_vault\_env) | A mapping of environment variable and Vault key. Default value of `vault_path` attribute is the `vault_path` variable. Default `secret_key` is the entry key. | <pre>map(object({<br>    vault_path = optional(string)<br>    secret_key = optional(string)<br>  }))</pre> | `{}` | no |
| <a name="input_vault_files"></a> [vault\_files](#input\_vault\_files) | Files to mount, content loaded from Vault. Default value of `vault_path` attribute is the `vault_path` variable. Default `secret_key` is the entry key. Default `host_filename` is the `sha256` hash of the entry key. Default `mount_path` is the entry key | <pre>map(object({<br>    vault_path    = optional(string)<br>    secret_key    = optional(string)<br>    host_filename = optional(string)<br>    mount_path    = optional(string)<br>  }))</pre> | `{}` | no |
| <a name="input_vault_path"></a> [vault\_path](#input\_vault\_path) | default Vault path to use when not provided in one of the `vault_env` or `vault_files` entries. Required when an entry in those inputs is missing the `vault_path` attribute | `string` | `null` | no |

### Outputs

No outputs.
<!-- END_TF_DOCS -->

## Examples
see implemented example in `/example` directory

### `files` attribute default values
```
files = {
  "/opt/lib/test.crt" = {
    content = "blabetiblou"

    # Default value is the entry key
    # mount_path = "/opt/lib/test.crt"

    # Default value is sha256 of the entry key
    # host_filename = sha256("opt/lib/test.crt")
  }
}
```
### `vault_files` default values
```
 vault_files = {
    "/etc/nginx/conf.d/nginx.conf.example" = {

      # secret_key = "/etc/nginx/conf.d/nginx.conf.example"

      # Default value is the entry key
      # mount_path = "/etc/nginx/conf.d/nginx.conf.example"

      # Default value is sha256 of the entry key
      # host_filename = sha256("opt/lib/test.crt")

      # vault_path = var.vault_path
    }
  }
```