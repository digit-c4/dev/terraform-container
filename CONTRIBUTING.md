## Generate documentation
```shell
terraform-docs markdown table --output-file README.md --output-mode inject --indent 3 .
```
