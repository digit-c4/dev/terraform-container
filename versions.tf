terraform {
  required_version = ">= 0.13"

  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }

    remote = {
      source = "widespot/remote"
      version = "0.2.1"
    }

    vault = {
      source = "hashicorp/vault"
      version = "3.23.0"
    }
  }
}
